class CreateCoordinatorGoals < ActiveRecord::Migration
  def change
    create_table :coordinator_goals do |t|
      t.integer :member_id
      t.date :start
      t.date :finish
      t.integer :quantity

      t.timestamps
    end
  end
end
