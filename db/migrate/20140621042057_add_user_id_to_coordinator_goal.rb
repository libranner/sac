class AddUserIdToCoordinatorGoal < ActiveRecord::Migration
  def change
  	add_column :coordinator_goals, :user_id, :integer
  end
end
