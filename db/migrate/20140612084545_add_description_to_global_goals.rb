class AddDescriptionToGlobalGoals < ActiveRecord::Migration
  def change
  	add_column :global_goals, :description, :text
  end
end
