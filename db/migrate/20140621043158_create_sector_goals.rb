class CreateSectorGoals < ActiveRecord::Migration
  def change
    create_table :sector_goals do |t|
      t.integer :sector_id
      t.date :start
      t.date :finish
      t.integer :quantity

      t.timestamps
    end
  end
end
