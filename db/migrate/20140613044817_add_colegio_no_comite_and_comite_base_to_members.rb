class AddColegioNoComiteAndComiteBaseToMembers < ActiveRecord::Migration
  def change
  	add_column :members, :no_comite, :integer
  	add_column :members, :comite_base, :string
  	add_column :members, :colegio_electoral, :string
  end
end
