class CreateGlobalGoals < ActiveRecord::Migration
  def change
    create_table :global_goals do |t|
      t.datetime :start
      t.datetime :finish
      t.integer :status_id
      t.integer :quantity
      t.integer :user_id

      t.timestamps
    end
  end
end
