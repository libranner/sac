class AddIsPresidentToMembers < ActiveRecord::Migration
  def change
    add_column :members, :is_president, :boolean
  end
end
