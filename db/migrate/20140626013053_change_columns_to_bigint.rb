class ChangeColumnsToBigint < ActiveRecord::Migration
  def change
  	change_column :members, :celular, :integer, limit: 8
  	change_column :members, :telephone, :integer, limit: 8
  end
end
