class ChangeMembersBirthdate < ActiveRecord::Migration
  def change
  	change_column :members, :birthdate, :date
  end
end
