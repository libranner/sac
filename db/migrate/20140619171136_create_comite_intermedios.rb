class CreateComiteIntermedios < ActiveRecord::Migration
  def change
    create_table :comite_intermedios do |t|
      t.string :name

      t.timestamps
    end
  end
end
