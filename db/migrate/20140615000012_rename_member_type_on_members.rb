class RenameMemberTypeOnMembers < ActiveRecord::Migration
  def change
  	rename_column :members, :member_type, :member_type_id
  end
end
