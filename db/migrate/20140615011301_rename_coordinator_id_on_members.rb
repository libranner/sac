class RenameCoordinatorIdOnMembers < ActiveRecord::Migration
  def change
  	rename_column :members, :coordinator_id, :member_id
  end
end
