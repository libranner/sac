class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.string :name
      t.string :lastname
      t.string :sex
      t.string :identification
      t.string :direction
      t.integer :sector_id
      t.integer :telephone
      t.integer :celular
      t.string :email
      t.datetime :birthdate
      t.boolean :is_pld_member

      t.timestamps
    end
  end
end
