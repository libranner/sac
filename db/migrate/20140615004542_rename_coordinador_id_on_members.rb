class RenameCoordinadorIdOnMembers < ActiveRecord::Migration
  def change
  	rename_column :members, :coordinador_id, :coordinator_id
  end
end
