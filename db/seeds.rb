# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts 'OCCUPATIONS'
#Occupation.create([{name: 'Ingeniero'}, {name: 'Arquitecto'}]) if Occupation.count == 0
Occupation.create([{name: 'Ingeniero/a'}, {name: 'Arquitecto/a'},
{name: "Médico"},{name: "Estilista"},{name: "Pintor"},
{name: "Psiquiatra"},{name: "Chofer"},
{name: "Enfermero/a"},{name: "Bioquímico/a"},{name: "Profesor/a"},
{name: "Contador/a"},{name: "Psicólogo/a"},{name: "Técnico/a"},
{name: "Sociólogo/a"},{name: "Abogado/a"},{name: "Secretario/a"},
{name: "Agricultor"}, {name: "Licenciado"},{name: "Actor/Actríz"},
{name: "Otro"},{name: "Plomero"}, {name: "Chef"},
{name: "Fotógrafo/a"}, {name: "Comerciante"}, {name: "Mecánico"},
{name: "Periodista"}, {name: "Músico"}, {name: "Locutor"},
{name: "Vendedor"}]) if Occupation.count == 0

puts 'COMITE INTERMEDIO'
ComiteIntermedio.create([
{name:'ANICETO MARTINEZ 1-A CANASTICA'},
{name:'ANICETO MARTINEZ 1-A-1 CANASTICA'},
{name:'ANICETO MARTINEZ 1-B CANASTICA'},
{name:'ANICETO MARTINEZ 1-C NAJAYO ARRIBA'},
{name:'ANICETO  MARTINEZ 1-D INGENIO NUEVO'},
{name:'ANICETO MARTINEZ 1-D-3 EL CERRO'},
{name:'ANICETO MARTINEZ 1-D-2 CANASTICA'},
{name:'ANICETO MARTINEZ 1-E INGENIO N./CAMBA'},
{name:'ANICETO  MARTINEZ 1-F CONANI'},
{name:'ANICETO MARTINEZ 1-F-1 NUEVA ESPERANZA'},
{name:'ANICETO MARTINEZ 1-F-2 SAN ISIDRO'},
{name:'ANICETO MARTINEZ 1-H CAMBITA STERLING'},
{name:'ANICETO MARTINEZ  1-H-1 KM.5'},
{name:'ANICETO AMRTINEZ 1-H-2 KM.4'},
{name:'JOSE LOPEZ A MADRE VIEJA SUR'},
{name:'JOSE LOPEZ B ESTEFANIA'},
{name:'JOSE LOPEZ C BARRIO METALURGICA'},
{name:'JOSE LOPEZ D MADRE VIEJA NORTE LA PIÑA'},
{name:'JOSE LOPEZ E MADRE VIEJA NORTE'},
{name:'JOSE LOPEZ F HATILLO EL ZUMBON'},
{name:'JOSE LOPEZ F-2'},
{name:'JOSE LOPEZ F-3'},
{name:'JOSE LOPEZ G HATILLO, HOJAS ANCHAS'},
{name:'JOSE LOPEZ G-2 HATILLO'},
{name:'JOSE LOPEZ G-3 SAN MIGUEL'},
{name:'JOSE LOPEZ H HATILLO, LA PRIVADA'},
{name:'JOSE LOPEZ H-3 HATILLO LA PRIVADA'},
{name:'JOSE LOPEZ H-2 HATILLO KM.26'},
{name:'MATA PALOMA, SANTA MARIA PTE. MUNICIPAL'},
{name:'ANICETO MARTINEZ 3-A, VILLA VALDEZ'},
{name:'ANICETO MARTINEZ 3-B, JERINGA'},
{name:'ANICETO MARTINEZ 3-C, LAS FLORES'},
{name:'ANICETO MARTINEZ 3-C-1, LAS FLORES'},
{name:'ANICETO MARTINEZ 3-D, LOS MOLINAS'},
{name:'ANICETO MARTINEZ 3-E, BARRIOS MOSCU'},
{name:'ANICETO MARTINEZ 3-F, EL POMIEL , VILLEGAS'},
{name:'ANICETO MARTINEZ 3-G, HATO DAMAS'},
{name:'ANICETO MARTINEZ 3-H, JAMEY'},
{name:'ANICETO MARTINEZ 4, CENTRO CIUDAD'},
{name:'ANICETO MARTINEZ 4-B PUEBLO NUEVO'},
{name:'ANICETO MARTINEZ 4-B-1 PUEBLO NUEVO'},
{name:'ANICETO MARTINEZ 4-B-2 CAÑADA HONDA'},
{name:'ANICETO MARTINEZ 4-C, PUEBLO NUEVO'},
{name:'ANICETO MARTINEZ 4-E, LAVA PIES'},
{name:'ANICETO MARTINEZ 4-F, SAINAGUA'}]) if ComiteIntermedio.count == 0

puts 'SECTORS'
Sector.create([
{name:'Barrio Puerto Rico'},
{name:'La Guandulera'},
{name:'Barrio 5 de abril'},
{name:'Lava Pies'},
{name:'Hato Damas'},
{name:'Jamey'},
{name:'San Francisco'},
{name:'El Cerro'},
{name:'Canastica'},
{name:'Villa Liberación'},
{name:'Villa Fundación'},
{name:'Villa Federico'},
{name:'Madre Vieja Norte'},
{name:'Los Molinas'},
{name:'La Suiza'},
{name:'Nueva Esperanza'},
{name:'San Isidro'},
{name:'Los Novas'},
{name:'Moscú'},
{name:'Las Flores'},
{name:'Canastica'},
{name:'Zona Verde'},
{name:'Jeringa'},
{name:'Las Flores'},
{name:'Semilla'},
{name:'Hatillo'},
{name:'Nigua'},
{name:'El Pomier'},
{name:'Los Cacaitos'},
{name:'La Toma'},
{name:'Los montones'},
{name:'Villa Valdez'},
{name:'Manue'},
{name:'Naranjo Dulce'},
{name:'Sainaguá'},
{name:'Rio piedra'},
{name:'Yaguate'},
{name:'Doña Ana'},
{name:'Najayo Arriba'},
{name:'favidrio'},
{name:'Sector Loyola'},
{name:'Villa Altagracia'},
{name:'Medina'},
{name:'Los Montones'}
]) if Sector.count == 0

puts 'MEMBER TYPES'
MemberType.create([{name: 'Coordinador'}, {name: 'Miembro'}]) if MemberType.count == 0

#Status.create([{name: 'Cancelada'}, {name: 'En espera'}, {name: 'En proceso'}, {name: 'Finalizada'}]) if Status.count ==0

puts 'DEFAULT USERS'
user = User.find_or_create_by_email :fullname => ENV['ADMIN_NAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
puts 'user: ' << user.fullname
user.add_role :admin

puts 'ROLES'

YAML.load(ENV['ROLES']).each do |role|
  Role.where(:name => role).first_or_create #(:without_protection => true)
  puts 'role: ' << role
end

