require "rails_helper"

RSpec.describe CoordinatorGoalsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/coordinator_goals").to route_to("coordinator_goals#index")
    end

    it "routes to #new" do
      expect(:get => "/coordinator_goals/new").to route_to("coordinator_goals#new")
    end

    it "routes to #show" do
      expect(:get => "/coordinator_goals/1").to route_to("coordinator_goals#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/coordinator_goals/1/edit").to route_to("coordinator_goals#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/coordinator_goals").to route_to("coordinator_goals#create")
    end

    it "routes to #update" do
      expect(:put => "/coordinator_goals/1").to route_to("coordinator_goals#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/coordinator_goals/1").to route_to("coordinator_goals#destroy", :id => "1")
    end

  end
end
