require "rails_helper"

RSpec.describe SectorGoalsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/sector_goals").to route_to("sector_goals#index")
    end

    it "routes to #new" do
      expect(:get => "/sector_goals/new").to route_to("sector_goals#new")
    end

    it "routes to #show" do
      expect(:get => "/sector_goals/1").to route_to("sector_goals#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/sector_goals/1/edit").to route_to("sector_goals#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/sector_goals").to route_to("sector_goals#create")
    end

    it "routes to #update" do
      expect(:put => "/sector_goals/1").to route_to("sector_goals#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/sector_goals/1").to route_to("sector_goals#destroy", :id => "1")
    end

  end
end
