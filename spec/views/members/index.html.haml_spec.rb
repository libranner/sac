require 'rails_helper'

RSpec.describe "members/index", :type => :view do
  before(:each) do
    assign(:members, [
      Member.create!(
        :name => "Name",
        :lastname => "Lastname",
        :sex => "Sex",
        :identification => "Identification",
        :direction => "Direction",
        :sector_id => 1,
        :telephone => 2,
        :celular => 3,
        :email => "Email",
        :is_pld_member => false
      ),
      Member.create!(
        :name => "Name",
        :lastname => "Lastname",
        :sex => "Sex",
        :identification => "Identification",
        :direction => "Direction",
        :sector_id => 1,
        :telephone => 2,
        :celular => 3,
        :email => "Email",
        :is_pld_member => false
      )
    ])
  end

  it "renders a list of members" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Lastname".to_s, :count => 2
    assert_select "tr>td", :text => "Sex".to_s, :count => 2
    assert_select "tr>td", :text => "Identification".to_s, :count => 2
    assert_select "tr>td", :text => "Direction".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
