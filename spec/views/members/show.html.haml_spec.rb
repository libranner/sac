require 'rails_helper'

RSpec.describe "members/show", :type => :view do
  before(:each) do
    @member = assign(:member, Member.create!(
      :name => "Name",
      :lastname => "Lastname",
      :sex => "Sex",
      :identification => "Identification",
      :direction => "Direction",
      :sector_id => 1,
      :telephone => 2,
      :celular => 3,
      :email => "Email",
      :is_pld_member => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Lastname/)
    expect(rendered).to match(/Sex/)
    expect(rendered).to match(/Identification/)
    expect(rendered).to match(/Direction/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/false/)
  end
end
