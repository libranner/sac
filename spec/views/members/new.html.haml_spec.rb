require 'rails_helper'

RSpec.describe "members/new", :type => :view do
  before(:each) do
    assign(:member, Member.new(
      :name => "MyString",
      :lastname => "MyString",
      :sex => "MyString",
      :identification => "MyString",
      :direction => "MyString",
      :sector_id => 1,
      :telephone => 1,
      :celular => 1,
      :email => "MyString",
      :is_pld_member => false
    ))
  end

  it "renders new member form" do
    render

    assert_select "form[action=?][method=?]", members_path, "post" do

      assert_select "input#member_name[name=?]", "member[name]"

      assert_select "input#member_lastname[name=?]", "member[lastname]"

      assert_select "input#member_sex[name=?]", "member[sex]"

      assert_select "input#member_identification[name=?]", "member[identification]"

      assert_select "input#member_direction[name=?]", "member[direction]"

      assert_select "input#member_sector_id[name=?]", "member[sector_id]"

      assert_select "input#member_telephone[name=?]", "member[telephone]"

      assert_select "input#member_celular[name=?]", "member[celular]"

      assert_select "input#member_email[name=?]", "member[email]"

      assert_select "input#member_is_pld_member[name=?]", "member[is_pld_member]"
    end
  end
end
