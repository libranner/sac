require 'rails_helper'

RSpec.describe "coordinator_goals/show", :type => :view do
  before(:each) do
    @coordinator_goal = assign(:coordinator_goal, CoordinatorGoal.create!(
      :member_id => 1,
      :quantity => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
