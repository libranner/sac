require 'rails_helper'

RSpec.describe "coordinator_goals/edit", :type => :view do
  before(:each) do
    @coordinator_goal = assign(:coordinator_goal, CoordinatorGoal.create!(
      :member_id => 1,
      :quantity => 1
    ))
  end

  it "renders the edit coordinator_goal form" do
    render

    assert_select "form[action=?][method=?]", coordinator_goal_path(@coordinator_goal), "post" do

      assert_select "input#coordinator_goal_member_id[name=?]", "coordinator_goal[member_id]"

      assert_select "input#coordinator_goal_quantity[name=?]", "coordinator_goal[quantity]"
    end
  end
end
