require 'rails_helper'

RSpec.describe "coordinator_goals/index", :type => :view do
  before(:each) do
    assign(:coordinator_goals, [
      CoordinatorGoal.create!(
        :member_id => 1,
        :quantity => 2
      ),
      CoordinatorGoal.create!(
        :member_id => 1,
        :quantity => 2
      )
    ])
  end

  it "renders a list of coordinator_goals" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
