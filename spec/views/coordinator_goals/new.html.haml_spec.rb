require 'rails_helper'

RSpec.describe "coordinator_goals/new", :type => :view do
  before(:each) do
    assign(:coordinator_goal, CoordinatorGoal.new(
      :member_id => 1,
      :quantity => 1
    ))
  end

  it "renders new coordinator_goal form" do
    render

    assert_select "form[action=?][method=?]", coordinator_goals_path, "post" do

      assert_select "input#coordinator_goal_member_id[name=?]", "coordinator_goal[member_id]"

      assert_select "input#coordinator_goal_quantity[name=?]", "coordinator_goal[quantity]"
    end
  end
end
