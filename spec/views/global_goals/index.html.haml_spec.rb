require 'rails_helper'

RSpec.describe "global_goals/index", :type => :view do
  before(:each) do
    assign(:global_goals, [
      GlobalGoal.create!(
        :status_id => 1,
        :quantity => 2,
        :user_id => 3
      ),
      GlobalGoal.create!(
        :status_id => 1,
        :quantity => 2,
        :user_id => 3
      )
    ])
  end

  it "renders a list of global_goals" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
