require 'rails_helper'

RSpec.describe "global_goals/show", :type => :view do
  before(:each) do
    @global_goal = assign(:global_goal, GlobalGoal.create!(
      :status_id => 1,
      :quantity => 2,
      :user_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
  end
end
