require 'rails_helper'

RSpec.describe "global_goals/new", :type => :view do
  before(:each) do
    assign(:global_goal, GlobalGoal.new(
      :status_id => 1,
      :quantity => 1,
      :user_id => 1
    ))
  end

  it "renders new global_goal form" do
    render

    assert_select "form[action=?][method=?]", global_goals_path, "post" do

      assert_select "input#global_goal_status_id[name=?]", "global_goal[status_id]"

      assert_select "input#global_goal_quantity[name=?]", "global_goal[quantity]"

      assert_select "input#global_goal_user_id[name=?]", "global_goal[user_id]"
    end
  end
end
