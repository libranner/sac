require 'rails_helper'

RSpec.describe "sector_goals/show", :type => :view do
  before(:each) do
    @sector_goal = assign(:sector_goal, SectorGoal.create!(
      :sector_id => 1,
      :quantity => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
