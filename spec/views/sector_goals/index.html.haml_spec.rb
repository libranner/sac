require 'rails_helper'

RSpec.describe "sector_goals/index", :type => :view do
  before(:each) do
    assign(:sector_goals, [
      SectorGoal.create!(
        :sector_id => 1,
        :quantity => 2
      ),
      SectorGoal.create!(
        :sector_id => 1,
        :quantity => 2
      )
    ])
  end

  it "renders a list of sector_goals" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
