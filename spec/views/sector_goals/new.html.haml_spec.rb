require 'rails_helper'

RSpec.describe "sector_goals/new", :type => :view do
  before(:each) do
    assign(:sector_goal, SectorGoal.new(
      :sector_id => 1,
      :quantity => 1
    ))
  end

  it "renders new sector_goal form" do
    render

    assert_select "form[action=?][method=?]", sector_goals_path, "post" do

      assert_select "input#sector_goal_sector_id[name=?]", "sector_goal[sector_id]"

      assert_select "input#sector_goal_quantity[name=?]", "sector_goal[quantity]"
    end
  end
end
