require 'rails_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

RSpec.describe CoordinatorGoalsController, :type => :controller do

  # This should return the minimal set of attributes required to create a valid
  # CoordinatorGoal. As you add validations to CoordinatorGoal, be sure to
  # adjust the attributes here as well.
  let(:valid_attributes) {
    skip("Add a hash of attributes valid for your model")
  }

  let(:invalid_attributes) {
    skip("Add a hash of attributes invalid for your model")
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # CoordinatorGoalsController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET index" do
    it "assigns all coordinator_goals as @coordinator_goals" do
      coordinator_goal = CoordinatorGoal.create! valid_attributes
      get :index, {}, valid_session
      expect(assigns(:coordinator_goals)).to eq([coordinator_goal])
    end
  end

  describe "GET show" do
    it "assigns the requested coordinator_goal as @coordinator_goal" do
      coordinator_goal = CoordinatorGoal.create! valid_attributes
      get :show, {:id => coordinator_goal.to_param}, valid_session
      expect(assigns(:coordinator_goal)).to eq(coordinator_goal)
    end
  end

  describe "GET new" do
    it "assigns a new coordinator_goal as @coordinator_goal" do
      get :new, {}, valid_session
      expect(assigns(:coordinator_goal)).to be_a_new(CoordinatorGoal)
    end
  end

  describe "GET edit" do
    it "assigns the requested coordinator_goal as @coordinator_goal" do
      coordinator_goal = CoordinatorGoal.create! valid_attributes
      get :edit, {:id => coordinator_goal.to_param}, valid_session
      expect(assigns(:coordinator_goal)).to eq(coordinator_goal)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new CoordinatorGoal" do
        expect {
          post :create, {:coordinator_goal => valid_attributes}, valid_session
        }.to change(CoordinatorGoal, :count).by(1)
      end

      it "assigns a newly created coordinator_goal as @coordinator_goal" do
        post :create, {:coordinator_goal => valid_attributes}, valid_session
        expect(assigns(:coordinator_goal)).to be_a(CoordinatorGoal)
        expect(assigns(:coordinator_goal)).to be_persisted
      end

      it "redirects to the created coordinator_goal" do
        post :create, {:coordinator_goal => valid_attributes}, valid_session
        expect(response).to redirect_to(CoordinatorGoal.last)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved coordinator_goal as @coordinator_goal" do
        post :create, {:coordinator_goal => invalid_attributes}, valid_session
        expect(assigns(:coordinator_goal)).to be_a_new(CoordinatorGoal)
      end

      it "re-renders the 'new' template" do
        post :create, {:coordinator_goal => invalid_attributes}, valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      let(:new_attributes) {
        skip("Add a hash of attributes valid for your model")
      }

      it "updates the requested coordinator_goal" do
        coordinator_goal = CoordinatorGoal.create! valid_attributes
        put :update, {:id => coordinator_goal.to_param, :coordinator_goal => new_attributes}, valid_session
        coordinator_goal.reload
        skip("Add assertions for updated state")
      end

      it "assigns the requested coordinator_goal as @coordinator_goal" do
        coordinator_goal = CoordinatorGoal.create! valid_attributes
        put :update, {:id => coordinator_goal.to_param, :coordinator_goal => valid_attributes}, valid_session
        expect(assigns(:coordinator_goal)).to eq(coordinator_goal)
      end

      it "redirects to the coordinator_goal" do
        coordinator_goal = CoordinatorGoal.create! valid_attributes
        put :update, {:id => coordinator_goal.to_param, :coordinator_goal => valid_attributes}, valid_session
        expect(response).to redirect_to(coordinator_goal)
      end
    end

    describe "with invalid params" do
      it "assigns the coordinator_goal as @coordinator_goal" do
        coordinator_goal = CoordinatorGoal.create! valid_attributes
        put :update, {:id => coordinator_goal.to_param, :coordinator_goal => invalid_attributes}, valid_session
        expect(assigns(:coordinator_goal)).to eq(coordinator_goal)
      end

      it "re-renders the 'edit' template" do
        coordinator_goal = CoordinatorGoal.create! valid_attributes
        put :update, {:id => coordinator_goal.to_param, :coordinator_goal => invalid_attributes}, valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested coordinator_goal" do
      coordinator_goal = CoordinatorGoal.create! valid_attributes
      expect {
        delete :destroy, {:id => coordinator_goal.to_param}, valid_session
      }.to change(CoordinatorGoal, :count).by(-1)
    end

    it "redirects to the coordinator_goals list" do
      coordinator_goal = CoordinatorGoal.create! valid_attributes
      delete :destroy, {:id => coordinator_goal.to_param}, valid_session
      expect(response).to redirect_to(coordinator_goals_url)
    end
  end

end
