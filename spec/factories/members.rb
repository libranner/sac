# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :member do
    name "MyString"
    lastname "MyString"
    sex "MyString"
    identification "MyString"
    direction "MyString"
    sector_id 1
    telephone 1
    celular 1
    email "MyString"
    birthdate "2014-06-03 00:50:30"
    is_pld_member false
  end
end
