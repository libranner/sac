# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sector_goal do
    sector_id 1
    start "2014-06-21"
    finish "2014-06-21"
    quantity 1
  end
end
