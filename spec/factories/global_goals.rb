# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :global_goal do
    start "2014-06-04 23:23:06"
    finish "2014-06-04 23:23:06"
    status_id 1
    quantity 1
    user_id 1
  end
end
