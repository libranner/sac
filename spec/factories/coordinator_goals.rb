# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :coordinator_goal do
    member_id 1
    start "2014-06-21"
    finish "2014-06-21"
    quantity 1
  end
end
