json.array!(@global_goals) do |global_goal|
  json.extract! global_goal, :id, :start, :finish, :quantity, :user_id
  json.url global_goal_url(global_goal, format: :json)
end
