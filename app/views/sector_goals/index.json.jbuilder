json.array!(@sector_goals) do |sector_goal|
  json.extract! sector_goal, :id, :sector_id, :start, :finish, :quantity
  json.url sector_goal_url(sector_goal, format: :json)
end
