json.array!(@coordinator_goals) do |coordinator_goal|
  json.extract! coordinator_goal, :id, :member_id, :start, :finish, :quantity
  json.url coordinator_goal_url(coordinator_goal, format: :json)
end
