json.array!(@members) do |member|
  json.extract! member, :id, :name, :lastname, :sex, :identification, :direction, :sector_id, :telephone, :celular, :email, :birthdate, :is_pld_member
  json.url member_url(member, format: :json)
end
