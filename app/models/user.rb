class User < ActiveRecord::Base
  	rolify
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  	devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

	has_many :global_goals
	has_many :members
	has_many :coordinator_goals
	has_many :sector_goals
  #attr_accessible :name, :email, :password, :password_confirmation, :remember_me
end
