class GlobalGoal < ActiveRecord::Base
	#attr_accessible :start, :finish, :status_id, :quantity, :user_id, :js_start, :js_finish
	belongs_to :user
	belongs_to :state
	validates :start, presence: {message: 'Debe indicar la fecha de inicio.'}
	validates :finish, presence: {message: 'Debe indicar la fecha de finalización.'}
  	validates :description, presence: {message: 'Debe indicar la descripción.'}
  	validates :quantity, presence: {message: 'Debe indicar la cantidad.'}  	
  	validates :user_id, presence: {message: 'Debe indicar el usuario.'}

	validate :greater_start

	def greater_start
  	errors.add(:start,'La fecha de inicio debe ser menor que la final.') if start.nil? || finish.nil? || self.start >= self.finish
	end

	def quantity_added
		Member.where(['DATE(created_at) BETWEEN ? AND ?', start, finish]).count
	end

	def percent
		value = 0
		value = (quantity_added.to_f/quantity.to_f) * 100 if quantity > 0 && quantity_added > 0
		value = 100 if value > 100
		value
	end

	#Associations
	belongs_to :status
end
