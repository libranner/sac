
class Member < ActiveRecord::Base
	belongs_to :sector
	belongs_to :user
	belongs_to :member_type
	belongs_to :comite_intermedio
	belongs_to :occupation
	has_many :coordinator_goals
	belongs_to :coordinator, class_name: 'Member', foreign_key: :member_id
	has_many :members #, through: :coordinator
	#scope :coordinators, :order => "name DESC"
	#scope :coordinators, -> { where(member_type_id: 2) }

	validates :no_comite, presence: {message: 'Debe indicar el número del comité de base.'}, if: :is_member?
	#validates :comite_base, presence: {message: 'Debe indicar el comité de base.'}, if: :is_member?
	validates :colegio_electoral, presence: {message: 'Debe indicar el colegio electoral.'}
	validates :comite_intermedio_id, presence: {message: 'Debe indicar el cómite intermedio.'}, if: :is_member?

	validates :name, presence: {message: 'Debe indicar el nombre.'}
	validates :lastname, presence: {message: 'Debe indicar el apellido.'}
	validates :sex, presence: {message: 'Debe indicar el sexo.'}
	validates :identification, presence: {message: 'Debe indicar la cédula.'}, if: :is_adult?
	validates :direction, presence: {message: 'Debe indicar la dirección.'}
	validates :sector_id, presence: {message: 'Debe indicar el sector.'}
	validates :telephone, presence: {message: 'Debe indicar el teléfono.'}
	validates :celular, presence: {message: 'Debe indicar el celular.'}
	validates :email, presence: {message: 'Debe indicar el email.'}
	validates :celular, presence: {message: 'Debe indicar el celular.'}
	validates :occupation_id, presence: {message: 'Debe indicar la profesión.'}

	validates :user_id, presence: {message: 'Debe indicar el usuario.'}
	validates :member_type_id, presence: {message: 'Debe indicar el tipo de miembro.'}
	validates :birthdate, presence: {message: 'Debe indicar la fecha de nacimiento.'}

	validates :member_id, presence: {message: 'Debe indicar el coordinador.'}, if: :is_coordinator?

	validates :identification, uniqueness: {message: 'Ya existe un miembro con esa cédula.'}, if: :is_adult? && :has_identification?

	validates :identification, numericality: { only_integer: true, message: 'Sólo debe ingresar números en el campo cédula.' }, if: :is_adult?
	validates :identification, length: {is: 11, message:'Cédula debe tener 11 dígitos.'}, if: :is_adult?
	validates :celular, length: {is: 10, message:'Celular debe tener 10 dígitos.'}
	validates :telephone, length: {is: 10, message:'Teléfono debe tener 10 dígitos.'}

	validate :has_valid_identification?, if: :is_adult?

	scope :coordinators, -> {where(:member_type_id => 1).order("name DESC")}


	def is_adult?
  		age = (Date.today - birthdate).to_i / 365
  		age >= 18
	end

	def has_identification?
		identification && identification != ''
	end

	def has_valid_identification?
		if  identification.nil? || !identification.numeric? || identification.length != 11
			errors.add(:identification,'Cédula inválida.')
		else
			peso = '1212121212'
			suma = 0
			division = 0
			for i in 0..10 
				mul = Integer(identification[i].to_f * peso[i].to_f)
				while mul > 0 do
					v = Integer(mul % 10)
					suma += v
					mul = Integer(mul /= 10)
				end
			end

			division = Integer(suma / 10) * 10
			division += 10 if division < suma
			digito = division - suma
			
			errors.add(:identification,'Cédula inválida.') if digito != identification[10].to_f
		end
	end


	def is_member?
		is_pld_member
	end

	def is_coordinator?
		member_type_id == 2
	end

	def self.search params
	      if !params['/members'][:is_pld_member].nil? &&  params['/members'][:is_pld_member] == '1'
	        members = Member.where("is_pld_member = ?", true)
	      else
	        members = Member.all
	      end   
	      
	      if !params['/members'][:name].nil?
	        members = members.find_with_name params['/members'][:name]
	      elsif !params['/members'][:identification].nil?
	        members = members.where("identification = ?", params['/members'][:identification])
	      elsif !params['/members'][:sector_id].nil?
	          sector = params['/members'][:sector_id]
	          occupation = params['/members'][:occupation_id]
	          sex = params['/members'][:sex]
	          coordinator = params['/members'][:coordinator_id]
	          member_type = params['/members'][:member_type_id]
	          members = members.where("sector_id = ?", sector) if sector != '0'
	          members = members.where("occupation_id = ?", occupation) if occupation != '0'
	          members = members.where("sex = ?", sex) if sex != ''
	          members = members.where("member_id = ?", coordinator) if coordinator != '0'
	          members = members.where("member_type_id = ?", member_type) if member_type != '0'
	      end

	      members.order("name").page(params[:page]).per_page(15)
	end

	def self.find_with_name(name)
		Member.where("name LIKE ? OR lastname LIKE ?", "%#{name}%", "%#{name}%").order("name DESC")
	end
end
