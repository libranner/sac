class Sector < ActiveRecord::Base
	has_many :members
	has_many :sector_goals

	validates :name, uniqueness: true
end
