module FormattingHelper
	def identification_formatter(number) 
		return 'Inválida' if number.length != 11
		"#{number[0..2]}-#{number[3..9]}-#{number[10]}"
	end

	def phone_number_formatter(phone_number)
		number = phone_number.to_s
		return 'Inválido' unless (10..11).include? number.length

		if number.length == 10
			"(#{number[0..2]})-#{number[3..5]}-#{number[6..9]}"
		else
			"1-(#{number[1..3]})-#{number[4..6]}-#{number[7..10]}"
		end
	end
end