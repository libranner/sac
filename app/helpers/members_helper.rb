module MembersHelper

	def valid_identification?(identification)
		if  identification.nil? || !identification.numeric? || identification.length != 11
			false
		else
			peso = '1212121212'
			suma = 0
			division = 0
			for i in 0..10 
				mul = Integer(identification[i].to_f * peso[i].to_f)
				while mul > 0 do
					v = Integer(mul % 10)
					suma += v
					mul = Integer(mul /= 10)
				end
			end

			division = Integer(suma / 10) * 10
			division += 10 if division < suma
			digito = division - suma
			
			true if digito != identification[10].to_f
		end
	end

	def objects_to_map(objects, blank_text)
		@result = []
    	objects.each { |rec| @result << [rec.name, rec.id]}
    	@result << [blank_text, 0]
    	@result.sort_by { |name, id| name }
	end

end
