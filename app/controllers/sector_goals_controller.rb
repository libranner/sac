class SectorGoalsController < ApplicationController
  load_and_authorize_resource
  before_action :set_sector_goal, only: [:show, :edit, :update, :destroy]

  # GET /sector_goals
  # GET /sector_goals.json
  def index
    @sector_goals = SectorGoal.order('start').page(params[:page]).per_page(15)
  end

  # GET /sector_goals/1
  # GET /sector_goals/1.json
  def show
  end

  # GET /sector_goals/new
  def new
    @sector_goal = SectorGoal.new
  end

  # GET /sector_goals/1/edit
  def edit
  end

  # POST /sector_goals
  # POST /sector_goals.json
  def create
    @sector_goal = SectorGoal.new(sector_goal_params)
    @sector_goal.user = current_user
    respond_to do |format|
      if @sector_goal.save
        format.html { redirect_to @sector_goal, notice: 'Meta por sector creada satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @sector_goal }
      else
        format.html { render action: 'new' }
        format.json { render json: @sector_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sector_goals/1
  # PATCH/PUT /sector_goals/1.json
  def update
    respond_to do |format|
      if @sector_goal.update(sector_goal_params)
        format.html { redirect_to @sector_goal, notice: 'Meta por sector actualizada satisfactoriamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @sector_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sector_goals/1
  # DELETE /sector_goals/1.json
  def destroy
    @sector_goal.destroy
    respond_to do |format|
      format.html { redirect_to sector_goals_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sector_goal
      @sector_goal = SectorGoal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sector_goal_params
      params.require(:sector_goal).permit(:sector_id, :start, :finish, :quantity, :user_id)
    end
end
