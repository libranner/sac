class CoordinatorGoalsController < ApplicationController
  load_and_authorize_resource
  before_action :set_coordinator_goal, only: [:show, :edit, :update, :destroy]

  # GET /coordinator_goals
  # GET /coordinator_goals.json
  def index
    @coordinator_goals = []
    if current_user  
      if (current_user.has_role? :coordinador)
        @coordinator_goals = CoordinatorGoal.includes('member').where("members.email" => current_user.email).order('start').page(params[:page]).per_page(15)
      elsif (current_user.has_role? :admin)
        @coordinator_goals = CoordinatorGoal.order('start').page(params[:page]).per_page(15)
      end
    end
  end

  # GET /coordinator_goals/1
  # GET /coordinator_goals/1.json
  def show
  end

  # GET /coordinator_goals/new
  def new
    @coordinator_goal = CoordinatorGoal.new
  end

  # GET /coordinator_goals/1/edit
  def edit
  end

  # POST /coordinator_goals
  # POST /coordinator_goals.json
  def create
    @coordinator_goal = CoordinatorGoal.new(coordinator_goal_params)
    @coordinator_goal.user = current_user
    respond_to do |format|
      if @coordinator_goal.save
        format.html { redirect_to @coordinator_goal, notice: 'La meta por coordinador fue creada satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @coordinator_goal }
      else
        format.html { render action: 'new' }
        format.json { render json: @coordinator_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /coordinator_goals/1
  # PATCH/PUT /coordinator_goals/1.json
  def update
    respond_to do |format|
      if @coordinator_goal.update(coordinator_goal_params)
        format.html { redirect_to @coordinator_goal, notice: 'La meta por coordinador fue actualizada satisfactoriamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @coordinator_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /coordinator_goals/1
  # DELETE /coordinator_goals/1.json
  def destroy
    @coordinator_goal.destroy
    respond_to do |format|
      format.html { redirect_to coordinator_goals_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coordinator_goal
      @coordinator_goal = CoordinatorGoal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coordinator_goal_params
      params.require(:coordinator_goal).permit(:member_id, :start, :finish, :quantity, :user_id)
    end
end
