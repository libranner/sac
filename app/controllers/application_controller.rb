class ApplicationController < ActionController::Base
	before_filter :configure_permitted_parameters, if: :devise_controller?
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  	protect_from_forgery with: :exception
	helper :formatting

before_filter do
  resource = controller_name.singularize.to_sym
  method = "#{resource}_params"
  params[resource] &&= send(method) if respond_to?(method, true)
end

rescue_from CanCan::AccessDenied do |exception|
  flash[:error] = (current_user.nil?) ? "Debes iniciar sesión para continuar." : "No tienes permisos para acceder."
  
  # if current_user.nil?
  #   redirect_to '/'
  # elsif !(current_user.has_role? :admim)
  #   redirect_to members_path
  # end
  redirect_to '/'
end

private
def mobile_device?
    request.user_agent =~ /Mobile|webOS/
end
helper_method :mobile_device?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit(:login, :email, :username, :fullname) }
  end

end
