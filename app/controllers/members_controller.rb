class MembersController < ApplicationController

  include MembersHelper
  
  before_action :set_member, only: [:show, :edit, :update, :destroy]

  load_and_authorize_resource
  
  # GET /members
  # GET /members.json
  def index
    
    if params['/members'].nil? 
      @members = Member.order("name").page(params[:page]).per_page(15)
    else
      @members = Member.search params
    end

    respond_to do |format|
      format.html do
        #Cargar data para búsqueda
        @coordinators = objects_to_map(Member.coordinators, '-Todos los coordinadores-')
        @sectors = objects_to_map(Sector.all, '-Todos los sectores-')
        @occupations = objects_to_map(Occupation.all, '-Todas las ocupaciones-')
        @member_types = objects_to_map(MemberType.all, '-Todos los miembros-')
      end
      format.pdf do
        render :pdf => "report", :layout => 'pdf.html.haml', orientation: 'Landscape'
      end
    end

  end

  # GET /members/1
  # GET /members/1.json
  def show
  end

  # GET /members/new
  def new
    @member = Member.new
  end

  # GET /members/1/edit
  def edit
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)
    @member.user = current_user
    respond_to do |format|
      if @member.save
        format.html { redirect_to members_path, notice: 'Miembro creado satisfactoriamente.' }
        format.json { render action: 'show', status: :created, location: @member }
      else
        format.html { render action: 'new' }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    respond_to do |format|
      if @member.update(member_params)
        format.html { redirect_to @member, notice: 'Miembro actualizado satisfactoriamente.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @member.destroy
    respond_to do |format|
      format.html { redirect_to members_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_member
      @member = Member.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def member_params
      params.require(:member).permit(:user_id, :member_type_id, :name, :lastname, :sex, :identification, 
        :direction, :sector_id, :telephone, :celular, :email, :birthdate, :is_pld_member, 
        :no_comite, :is_president, :colegio_electoral, :coordinator_idm, :comite_intermedio_id, :occupation_id, :member_id)
    end
end
