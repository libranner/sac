class UsersController < ApplicationController
	#load_and_authorize_resource
	before_filter :authenticate_user!

	def index
		authorize! :index, @user, :message => 'Usted no está autorizado.'
	    @users = User.all
	end

	def show
	    @user = User.find(params[:id])
	end
	  
	def update
	    authorize! :update, @user, :message => 'Usted no está autorizado.'
	    @user = User.find(params[:id])
	    if @user.update_attributes(params[:user].permit(:role_ids))
	      #UserMailer.role_changed(@user).deliver
	      redirect_to users_path, :notice => "Usuario actualizado."
	    else
	      redirect_to users_path, :alert => "No se ha podido actualizar los datos."
	    end
	end
	    
	def destroy
	   authorize! :destroy, @user, :message => 'Usted no está autorizado.'
	   user = User.find(params[:id])
	   unless user == current_user
	     user.destroy
	     redirect_to users_path, :notice => "Usuario eliminado."
	   else
	     redirect_to users_path, :notice => "No puede eliminarse usted mismo."
	   end
	end

	private
		def user_params
	      params.require(:user).permit(:role_ids, :fullname)
	    end
end
